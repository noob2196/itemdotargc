import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreInforComponent } from './core-infor.component';

describe('CoreInforComponent', () => {
  let component: CoreInforComponent;
  let fixture: ComponentFixture<CoreInforComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoreInforComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoreInforComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
