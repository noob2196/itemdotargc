import { Component, OnInit } from "@angular/core";
import { DataService } from "../data.service";
@Component({
  selector: "app-core-infor",
  templateUrl: "./core-infor.component.html",
  styleUrls: ["./core-infor.component.css"]
})
export class CoreInforComponent implements OnInit {
  item: any;

  constructor(private data: DataService) {}

  ngOnInit() {
    this.data.currentMessage.subscribe(message => {
      this.item = message;
      console.log(this.item);
    });
  }
}
