import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-core-infor-item-usedin',
  templateUrl: './core-infor-item-usedin.component.html',
  styleUrls: ['./core-infor-item-usedin.component.css']
})
export class CoreInforItemUsedinComponent implements OnInit {

  @Input() item: any;

  constructor() { }

  ngOnInit() {
  }

}
