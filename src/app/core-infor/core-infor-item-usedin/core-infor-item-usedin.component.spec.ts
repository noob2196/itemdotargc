import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreInforItemUsedinComponent } from './core-infor-item-usedin.component';

describe('CoreInforItemUsedinComponent', () => {
  let component: CoreInforItemUsedinComponent;
  let fixture: ComponentFixture<CoreInforItemUsedinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoreInforItemUsedinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoreInforItemUsedinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
