import { Component, OnInit, Input } from "@angular/core";
import { DataService } from "../data.service";
@Component({
  selector: "app-core-item-inner",
  templateUrl: "./core-item-inner.component.html",
  styleUrls: ["./core-item-inner.component.css"]
})
export class CoreItemInnerComponent implements OnInit {
  @Input() item: any;
  constructor(private data: DataService) {}

  ngOnInit() {}

  getValue(value: any) {
     this.data.changeMessage(value);
  }
}
