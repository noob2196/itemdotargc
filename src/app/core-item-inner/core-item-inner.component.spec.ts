import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreItemInnerComponent } from './core-item-inner.component';

describe('CoreItemInnerComponent', () => {
  let component: CoreItemInnerComponent;
  let fixture: ComponentFixture<CoreItemInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoreItemInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoreItemInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
