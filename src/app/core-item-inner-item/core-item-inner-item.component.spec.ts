import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreItemInnerItemComponent } from './core-item-inner-item.component';

describe('CoreItemInnerItemComponent', () => {
  let component: CoreItemInnerItemComponent;
  let fixture: ComponentFixture<CoreItemInnerItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoreItemInnerItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoreItemInnerItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
