import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-core-item-inner-item',
  templateUrl: './core-item-inner-item.component.html',
  styleUrls: ['./core-item-inner-item.component.css']
})
export class CoreItemInnerItemComponent implements OnInit {
  @Input() imgUrl:String;
  constructor() { }

  ngOnInit() {
  }

}
