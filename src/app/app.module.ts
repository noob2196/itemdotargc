import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CoreItemComponent } from './core-item/core-item.component';
import { CoreInforComponent } from './core-infor/core-infor.component';
import { CoreItemInnerComponent } from './core-item-inner/core-item-inner.component';
import { CoreItemInnerItemComponent } from './core-item-inner-item/core-item-inner-item.component';
import { CoreInforItemUsedinComponent } from './core-infor/core-infor-item-usedin/core-infor-item-usedin.component';

@NgModule({
  declarations: [
    AppComponent,
    CoreItemComponent,
    CoreInforComponent,
    CoreItemInnerComponent,
    CoreItemInnerItemComponent,
    CoreInforItemUsedinComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
