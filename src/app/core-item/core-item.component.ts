import { Component, OnInit } from "@angular/core";
import { DataService } from "../data.service";

@Component({
  selector: "app-core-item",
  templateUrl: "./core-item.component.html",
  styleUrls: ["./core-item.component.css"]
})
export class CoreItemComponent implements OnInit {
  message: string;
//   listItem = [
//     {
//       id: 1,
//       name: "Protectorate",
//       items: [
//         {
//           id: 1,
//           requiresItem: true,
//           name: "Assault Cuirass",
//           imgUrl: "assets/Protectorate/AssaultCuirass.PNG",
//           price: 5250,
//           bonus: `+10 armor, +30 attack speed
// +5 armor aura, +25 attack speed aura (allies), -5 armor aura(enemy)`,
//           requires: [
//             {
//               nameItem: "Platemail",
//               imgUrl: "assets/WeaponsDealer/PlateMail.jpg",
//               price: 1400
//             },
//             {
//               nameItem: "Chainmail",
//               imgUrl: "assets/WeaponsDealer/Chainmail.jpg",
//               price: 550
//             },
//             {
//               nameItem: "Hyperstone",
//               imgUrl: "assets/LeragastheVile/Hyperstone.jpg",
//               price: 2000
//             },
//             {
//               nameItem: "Recipe",
//               imgUrl: "assets/repice.png",
//               price: 1400
//             }
//           ]
//         },
//         {
//           id: 2,
//           requiresItem: true,
//           name: "Heart of Tarrasque",
//           imgUrl: "assets/Protectorate/HeartofTarrasque.jpg",
//           price: 5500,
//           bonus: "+45 Strength, +250 HP, +7% HP regen out of combat",
//           requires: [
//             {
//               nameItem: "Messerschmidt's Reaver",
//               imgUrl: "assets/LeragastheVile/Messerschmidt'sReaver.jpg",
//               price: 3000
//             },
//             {
//               nameItem: "Vitality Booster",
//               imgUrl: "assets/LeragastheVile/VitalityBooster.jpg",
//               price: 1100
//             },
//             {
//               nameItem: "Recipe",
//               imgUrl: "assets/repice.png",
//               price: 1400
//             }
//           ]
//         },
//         {
//           id: 3,
//           requiresItem: true,
//           name: "Black King Bar",
//           imgUrl: "assets/Protectorate/BlackKingBar.jpg",
//           price: 1375,
//           bonus: "+10 Strength, +24 Damage, Avatar(active)",
//           requires: [
//             {
//               nameItem: "Ogre Axe",
//               imgUrl: "assets/SenaTheAccessorizer/OgreAxe.jpg",
//               price: 1000
//             },
//             {
//               nameItem: "Mithril Hammer",
//               imgUrl: "assets/WeaponsDealer/MithrilHammer.jpg",
//               price: 1600
//             },
//             {
//               nameItem: "Recipe",
//               imgUrl: "assets/repice.png",
//               price: 1375
//             }
//           ]
//         },
//         {
//           id: 4,
//           requiresItem: true,
//           name: "Crimson Guard",
//           imgUrl: "assets/Protectorate/CrimsonGuard.PNG",
//           price: 3550,
//           bonus: "+2 Strength, +2 Agility, +2 Intelligence, +250 HP, +8 HP regen, +5 Armor, 50% chance to block 70 damage Crimson Guard(active)",
//           requires: [
//             {
//               nameItem: "Vanguard",
//               imgUrl: "assets/Protectorate/Vanguard.JPG",
//               price: 2150
//             },
//             {
//               nameItem: "Natherezim Buckler",
//               imgUrl: "assets/SupportiveVestment/NathrezimBuckler.jpg",
//               price: 800
//             },
//             {
//               nameItem: "Recipe",
//               imgUrl: "assets/repice.png",
//               price: 600
//             }
//           ]
//         },
//         {
//           id: 5,
//           requiresItem: true,
//           name: "Shiva's Guard",
//           imgUrl: "assets/Protectorate/Shiva'sGuard.jpg",
//           price: 4700,
//           bonus: "+30 Intelligence, +15 Armor, Freezing Aura Arctic Blast(active)",
//           requires: [
//             {
//               nameItem: "Platemail",
//               imgUrl: "assets/WeaponsDealer/PlateMail.jpg",
//               price: 1400
//             },
//             {
//               nameItem: "Mystic Staff",
//               imgUrl: "assets/LeragastheVile/MysticStaff.jpg",
//               price: 2700
//             },
//             {
//               nameItem: "Recipe",
//               imgUrl: "assets/repice.png",
//               price: 1375
//             }
//           ]
//         },
//         {
//           id: 6,
//           requiresItem: true,
//           name: "Bloodstone",
//           imgUrl: "assets/Protectorate/Bloodstone.jpg",
//           price: 4900,
//           bonus: "+475 HP, +425 mana, +200% mana regen, +9 HP regen, Bloodpact(passive), Sacrifice(active)",
//           requires: [
//             {
//               nameItem: "Soul Booster",
//               imgUrl: "assets/Protectorate/Soul Booster.jpg",
//               price: 3200
//             },
//             {
//               nameItem: "Soul Ring",
//               imgUrl: "assets/GatewayRelics/SoulRing.jpg",
//               price: 800
//             },
//             {
//               nameItem: "Recipe",
//               imgUrl: "assets/repice.png",
//               price: 900
//             }
//           ]
//         },
//         {
//           id: 7,
//           requiresItem: true,
//           name: "Linken's Sphere",
//           imgUrl: "assets/Protectorate/Linken's Sphere.jpg",
//           price: 4800,
//           bonus: "Blocks most targeted spells once every 16 seconds, +6 HP regen, +150% mana regen, +15 Agility, +15 Intelligence, +15 Strength, +15 Damage",
//           requires: [
//             {
//               nameItem: "Perseverance",
//               imgUrl: "assets/GatewayRelics/Perseverance.jpg",
//               price: 1700
//             },
//             {
//               nameItem: "UltimateOrb",
//               imgUrl: "assets/SenaTheAccessorizer/UltimateOrb.jpg",
//               price: 2100
//             },
//             {
//               nameItem: "Recipe",
//               imgUrl: "assets/repice.png",
//               price: 1000
//             }
//           ]
//         },
//         {
//           id: 8,
//           requiresItem: true,
//           name: "Vanguard",
//           imgUrl: "assets/Protectorate/Vanguard.JPG",
//           price: 2150,
//           bonus: "+8 HP regen, +250 Hit Points, 50% to block 70 damage(35 for ranged)",
//           requires: [
//             {
//               nameItem: "Ring of Health",
//               imgUrl: "assets/LeragastheVile/RingofHealth.jpg",
//               price: 850
//             },
//             {
//               nameItem: "Vitality Booster",
//               imgUrl: "assets/LeragastheVile/VitalityBooster.jpg",
//               price: 1100
//             },
//             {
//               nameItem: "Stout Shield",
//               imgUrl: "assets/WeaponsDealer/StoutShield.jpg",
//               price: 200
//             }
//           ]
//         },
//         {
//           id: 9,
//           requiresItem: true,
//           name: "Blade Mail",
//           imgUrl: "assets/Protectorate/BladeMail.jpg",
//           price: 2200,
//           bonus: "+6 Armor, +22 Damage, +10 Intelligence, Damage return(active)",
//           requires: [
//             {
//               nameItem: "Broadsword",
//               imgUrl: "assets/WeaponsDealer/Broadsword.jpg",
//               price: 1200
//             },
//             {
//               nameItem: "Chainmail",
//               imgUrl: "assets/WeaponsDealer/Chainmail.jpg",
//               price: 550
//             },
//             {
//               nameItem: "Robe of The Magi",
//               imgUrl: "assets/SenaTheAccessorizer/RobeofTheMagi.jpg",
//               price: 450
//             }
//           ]
//         },
//         {
//           id: 10,
//           requiresItem: true,
//           name: "Lotus Orb",
//           imgUrl: "assets/Protectorate/LotusOrb.PNG",
//           price: 4000,
//           bonus: "+5 HP regen, +125% MP regen, +250 mana, +10 Damage, +10 armor, Echo Shell(active)",
//           requires: [
//             {
//               nameItem: "Perseverance",
//               imgUrl: "assets/GatewayRelics/Perseverance.jpg",
//               price: 1700
//             },
//             {
//               nameItem: "BladeMail",
//               imgUrl: "assets/Protectorate/BladeMail.jpg",
//               price: 1400
//             },
//             {
//               nameItem: "Energy Booster",
//               imgUrl: "assets/LeragastheVile/EnergyBooster.jpg",
//               price: 900
//             }
//           ]
//         },
//         {
//           id: 11,
//           requiresItem: true,
//           name: "Hood of Defiance",
//           imgUrl: "assets/Protectorate/HoodofDefiance.jpg",
//           price: 1725,
//           bonus: "+30% Magic resistance, +8 HP regen, Self-barrier(active)",
//           requires: [
//             {
//               nameItem: "Cloak",
//               imgUrl: "assets/CacheofQuel-thelan/Planeswalker'sCloak.jpg",
//               price: 550
//             },
//             {
//               nameItem: "Ring of Health",
//               imgUrl: "assets/LeragastheVile/RingofHealth.jpg",
//               price: 850
//             },
//             {
//               nameItem: "Ring of Regeneration",
//               imgUrl: "assets/CacheofQuel-thelan/RingofRegeneration.jpg",
//               price: 325
//             }
//           ],
//         },
//         {
//           id: 12,
//           requiresItem: true,
//           name: "Manta Style",
//           imgUrl: "assets/Protectorate/MantaStyle.PNG",
//           price: 4950,
//           bonus: "+26 Agility, +10 Strength, +15 Attack spped, 10% Movement speed, Mirror Image(active)",
//           requires: [
//             {
//               nameItem: "Yasha",
//               imgUrl: "assets/EnchantedArtifacts/Yasha.jpg",
//               price: 2050
//             },
//             {
//               nameItem: "UltimateOrb",
//               imgUrl: "assets/SenaTheAccessorizer/UltimateOrb.jpg",
//               price: 2100
//             },
//             {
//               nameItem: "Recipe",
//               imgUrl: "assets/repice.png",
//               price: 800
//             }
//           ],
//         },
//       ]
//     }
//   ];
  listItem = [];
  constructor(private data: DataService) { }

  ngOnInit() {
    this.showData();
    this.data.currentMessage.subscribe(message => (this.message = message));
  }

  showData() {
    this.data.getConfig()
      .subscribe((data: any) => {
        this.listItem = data;
      });
  }
}
