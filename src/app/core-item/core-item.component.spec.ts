import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreItemComponent } from './core-item.component';

describe('CoreItemComponent', () => {
  let component: CoreItemComponent;
  let fixture: ComponentFixture<CoreItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoreItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoreItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
